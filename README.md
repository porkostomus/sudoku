# To build:

1. Install [Leiningen](https://leiningen.org/).
2. Clone this repo.
3. Type `lein run` from the project directory.
4. Point your browser to `localhost:8080`.