(defproject sudoku "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
		 [org.clojure/tools.macro "0.1.2"]
                 [org.clojure/core.logic "0.8.5"]
		 [ring/ring-jetty-adapter "1.4.0"]
		 [compojure "1.4.0"]
		 [ring/ring-defaults "0.1.2"]
                 [hiccup "1.0.5"]]
:main ^:skip-aot sudoku.core
  :uberjar-name "sudoku-standalone.jar"
  :plugins [[lein-ring "0.8.13"]]

  :profiles {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                                  [ring-mock "0.1.5"]]}
             :uberjar {:aot :all}})
