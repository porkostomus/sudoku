(ns sudoku.core
  (:require [reagent.core :as reagent :refer [atom]]))

(enable-console-print!)

(defn matrix []
  (zipmap (for [i (range 9)
        j (range 9)]
      [i j]) [3 8 2 7 6 5 1 9 4
	      1 4 5 3 2 9 6 7 8
	      6 7 9 1 4 8 2 3 5
	      2 1 3 4 5 6 7 8 9
	      4 5 6 8 9 7 3 1 2
	      7 9 8 2 1 3 4 5 6
	      5 2 1 9 3 4 8 6 7
	      8 6 4 5 7 1 9 2 3
	      9 3 7 6 8 2 5 4 1]))

;; Game state
;; TODO - Plug this with modal window

(defonce app-state
  (atom
    {:matrix (matrix)
     :selecting false}))

(defn rect-cell [x y]
  [:rect.cell
   {:x (+ 0.05 x) :width 0.9
    :y (+ 0.05 y) :height 0.9
    :fill "white"
    :stroke-width 0.025
    :on-click #(println "titi")
    :stroke "black"}])

(defn text-cell
  "The text to fill inside the rectangle"
  [x y]
  [:text
   {:x (+ 0.5 x) :width 1
    :y (+ 0.72 y) :height 1
    :text-anchor "middle"
    :on-click #(println "toto")
    :font-size 0.6}
   (str (get (:matrix @app-state) [x y]))])

(defn render-board
  "Render the board of the sudoku"
  []
  (into
    [:svg.board
     {:view-box "0 0 9 9"
      :shape-rendering "auto"
      :style {:max-height "500px"}}]
    (for [i (range 9)
          j (range 9)]
      [:g
       [rect-cell i j]
       [text-cell i j]]
      )))

(defn sudoku
  "Main rendering function"
  []
  [:div
   [:h1 "Sudoku"]
   [:div [render-board]]
   ])

(reagent/render
  [sudoku]
  (js/document.getElementById "app"))
